"use strict";

const salt = "lakith";

var express = require('express');

var router = express.Router();

var bycrypt = require('bcrypt-nodejs');

var bodyParser = require('body-parser');

var mongoose = require('mongoose');

var User = require('../models/user');

var Card = require('../models/card');

router.use(bodyParser.json());

router.use(bodyParser.urlencoded({extended: true}));

router.get('/', function (req, res)
{

    User.find({}, function (err, users)
    {
        if (err)
        {
            return res.status(500).send({
                message: "There was a problem finding the users.",
                status: "NO_RESULTS"
            });
        }

        res.status(200).send({
            results: users,
            status: "OK"
        });
    });
});

router.post('/', function (req, res)
{

    var hash = bycrypt.hashSync(req.body.password);

    var card = new Card({
        _id: new mongoose.Types.ObjectId,
        balance: 1000
    });


    card.save(function (error) {

        if (error)
        {
            return handleError(error);
        }

        var user = new User({
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                email: req.body.email,
                password: hash,
                card: card._id
        });

        user.save(function (err, user)
        {
            if (err)
            {
                return res.status(500).send({
                    message: "There was a problem adding the information to the database.",
                    status: "NO_RESULTS"
                });
            }

            res.status(200).send({
                status: "OK",
                results: user
            });
        });

    });



});

router.get('/:_id', function (req, res)
{

    User.findById(req.params._id, function (err, user)
    {

        if (err)
        {
            return res.status(500).send(err);
        }

        if (!user)
        {
            return res.status(404).send(
                {
                    "message": "No Results",
                    "error": err,
                    "status": "NO_RESULTS"
                });
        }

        res.status(200).send({
            results: user,
            status: "OK"
        });
    });

});

router.put('/:_id', function (req, res)
{

    User.findByIdAndUpdate(req.params._id, req.body, {new: true}, function (err, user) {
        if (err) {
            return res.status(500).send({
                message: "There was a problem updating the user.",
                error: err,
                status: "NO_RESULTS"
            });
        }

        res.status(200).send({
            status: "OK",
            results: user
        });
    });

});

router.delete('/:_id', function (req, res)
{

    User.findByIdAndRemove(req.params._id, function (err, user) {

        if (err) {
            return res.status(500).send({
                message: "There was a problem deleting the user.",
                error: err,
                status: "NO_RESULTS"
            });
        }

        res.status(200).send({
            message: "User " + user.first_name + " was deleted.",
            status: "OKAY"
        });
    });
});

module.exports = router;