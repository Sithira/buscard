"use strict";

var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({

    name: String,
    first_name: String,
    last_name: String,
    email: String,
    password: String,
    user_type: String,
    card: [{type: mongoose.Schema.ObjectId, ref: 'Card'}]

});

mongoose.model('User', UserSchema);

module.exports = mongoose.model('User');