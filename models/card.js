"use strict";

var mongoose = require('mongoose');

var CardSchema = new mongoose.Schema({

    balance: {
        type: Number
    }

});

mongoose.model('Card', CardSchema);

module.exports = mongoose.model('Card');