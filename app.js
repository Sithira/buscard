var express = require('express');

var app = express();

var db = require('./database/db');

var UserController = require('./controllers/usercontroller');

app.use('/api/users', UserController);

module.exports = app;